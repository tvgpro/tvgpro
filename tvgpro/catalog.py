from substanced.catalog import (
    catalog_factory,
    indexview,
    indexview_defaults,
    Field,
)
from substanced.event import subscribe_created
from substanced.root import Root

from tvgpro.playlist.channel import ChannelMapping
from tvgpro.xmltv.channel import XmlTvChannel
from tvgpro.xmltv.programme import XmlTvProgramme
from .blog import (
    BlogEntry,
)


@subscribe_created(Root)
def created(event):
    root = event.object
    catalogs = root['catalogs']
    catalogs.add_catalog('blog', update_indexes=True)
    # catalogs.add_catalog('xmltv', update_indexes=True)


@catalog_factory('blog')
class BlogCatalogFactory(object):
    pubdate = Field()


@indexview_defaults(catalog_name='blog')
class BlogCatalogViews(object):
    def __init__(self, resource):
        self.resource = resource

    @indexview(context=BlogEntry)
    # @indexview(context=Comment)
    def pubdate(self, default):
        return getattr(self.resource, 'pubdate', default)


@catalog_factory('xmltv')
class XmlTvIndexes(object):
    display_name = Field()
    channel_id = Field()
    start = Field()


@indexview_defaults(catalog_name='xmltv')
class TvgProCatalogViews(object):
    def __init__(self, resource):
        self.resource = resource

    @indexview(context=XmlTvChannel)
    def display_name(self, default):
        return getattr(self.resource, 'display_name', default)

    @indexview(context=XmlTvProgramme)
    def start(self, default):
        date = getattr(self.resource, 'start', default)
        if date is not default:
            date = date.isoformat()
        return date

    @indexview(context=XmlTvProgramme, index_name='channel_id', attr='channel_id')
    def channel_id(self, default):
        return getattr(self.resource, 'channel_id', default)


@catalog_factory('playlist')
class PlaylistIndexes(object):
    group_title = Field()
    name = Field()


@indexview_defaults(catalog_name='playlist')
class PlaylistCatalogViews(object):
    def __init__(self, resource):
        self.resource = resource

    @indexview(context=ChannelMapping)
    def group_title(self, default):
        return getattr(self.resource, 'group_title', default)

    @indexview(context=ChannelMapping)
    def channel_id(self, default):
        return getattr(self.resource, 'name', default)
