from datetime import datetime
from urllib.parse import urlparse, parse_qs

import colander
import deform
import deform.widget
import transaction
from defusedxml import ElementTree
from persistent import Persistent
from persistent.list import PersistentList
from persistent.mapping import PersistentMapping
from pyramid.httpexceptions import HTTPFound
from substanced.content import content
from substanced.folder import Folder, SequentialAutoNamingFolder
from substanced.form import FormView
from substanced.interfaces import ReferenceType
from substanced.objectmap import find_objectmap
from substanced.objectmap import multireference_sourceid_property
from substanced.property import PropertySheet
from substanced.schema import (
    Schema,
    NameSchemaNode,
    MultireferenceIdSchemaNode)
from substanced.sdi import mgmt_view
from substanced.util import (
    oid_of,
    find_index)
from substanced.util import (
    renamer,
    find_catalog,
)
from zope.interface import (
    Interface,
    implementer,
)

from tvgpro import get_from_url
from tvgpro.playlist.channel import ChannelSequencePropertySheet


@colander.deferred
def deferred_objectrefs(node, kw):
    context = kw['context']
    system_catalog = find_catalog(context, 'system')
    q = (
        system_catalog['content_type'].eq('XtrProvider')
        # & channel_oid in self.channel_oid
    )
    root = kw['request'].root
    L = []
    maximum = 50
    current = 0
    for name, obj in root.items():
        oid = obj.__oid__
        L.append((oid, name))
        current += 1
        if current >= maximum:
            break
    return deform.widget.Select2Widget(values=L)


class ReferenceSequence(colander.SequenceSchema):
    refid = colander.SchemaNode(
        colander.Int(),
        widget=deferred_objectrefs,
        preparer=int,
    )


class AccountSchema(Schema):
    username = colander.SchemaNode(
        colander.String(),
    )
    password = colander.SchemaNode(
        colander.String(),
    )
    channels = ReferenceSequence(
        widget=deform.widget.SequenceWidget(orderable=True)
    )


class AccountPropertySheet(PropertySheet):
    schema = AccountSchema()


@content(
    'XtrAccount',
    icon='glyphicon glyphicon-time',
    # add_view='add_programme',
    propertysheets=(
            ('', AccountPropertySheet),
    ),
    catalog=True,
)
class XtrAccount(Persistent):
    def __init__(self, username, password):
        self.username = username
        self.password = password
