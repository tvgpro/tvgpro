import logging
from datetime import datetime
from urllib.parse import urlparse, parse_qs

import colander
import deform
import deform.widget
import transaction
from defusedxml import ElementTree
from persistent.list import PersistentList
from persistent.mapping import PersistentMapping
from pyramid.httpexceptions import HTTPFound
from substanced.content import content
from substanced.folder import Folder, SequentialAutoNamingFolder
from substanced.form import FormView
from substanced.interfaces import ReferenceType
from substanced.objectmap import find_objectmap
from substanced.objectmap import multireference_sourceid_property
from substanced.property import PropertySheet
from substanced.schema import (
    Schema,
    NameSchemaNode,
    MultireferenceIdSchemaNode)
from substanced.sdi import mgmt_view
from substanced.util import (
    oid_of,
    find_index)
from substanced.util import (
    renamer,
    find_catalog,
)
from zope.interface import (
    Interface,
    implementer,
)

from tvgpro import get_from_url
from tvgpro.playlist.channel import ChannelSequencePropertySheet, ChannelMapping
from tvgpro.playlist.xtr.account import XtrAccount

log = logging.getLogger(__name__)

xtr_path = 'get.php?username=%s&password=%s&type=m3u_plus&output=ts'


def account_from_url(node, value):
    result = urlparse(value)
    if result.scheme == 'http':  # and result.path == '/get.php':
        args = parse_qs(result.query)
        _type = args['type']
        user = args['username'][0]
        password = args['password'][0]
        return dict(host=result.hostname, port=result.port, username=user, password=password, type=_type)


class XtrProviderSchema(Schema):
    domain = colander.SchemaNode(
        colander.String(),
    )
    port = colander.SchemaNode(
        colander.Int(),
        missing=80
    )


class XtrProviderPropertySheet(PropertySheet):
    schema = XtrProviderSchema()


def xtr_provider_columns(folder, subobject, request, default_columnspec):
    subobject_name = getattr(subobject, '__name__', str(subobject))
    objectmap = find_objectmap(folder)
    user_oid = getattr(subobject, '__creator__', None)
    m3u_group = getattr(subobject, 'group_title', None)
    if user_oid is not None:
        user = objectmap.object_for(user_oid)
        user_name = getattr(user, '__name__', 'anonymous')
    else:
        user_name = 'anonymous'

    def make_sorter(index_name):
        def sorter(folder, resultset, limit=None, reverse=False):
            index = find_index(folder, 'sdidemo', index_name)
            if index is None:
                return resultset
            return resultset.sort(index, limit=limit, reverse=reverse)

        return sorter

    return default_columnspec + [
        {'name': 'Title',
         'value': getattr(subobject, 'name', subobject_name),
         'sorter': make_sorter('title'),
         },
        {'name': 'Group',
         'value': m3u_group,
         },
        {'name': 'Creator',
         'value': user_name,
         }
    ]


@content(
    'XtrProvider',
    icon='glyphicon glyphicon-book',
    add_view='add_m3u_source',
    propertysheets=(
            ('Basic', XtrProviderPropertySheet),
    ),
    columns=xtr_provider_columns,
)
# @implementer(IM3USourceContent)
class XtrProvider(Folder):
    addables = ['XtrAccount']

    def __init__(self, domain, port):
        super(XtrProvider, self).__init__()
        self.domain = domain
        self.port = port
        self['accounts'] = Folder()
        # self.accounts = Folder()

    def __sdi_addable__(self, context, introspectable):
        return introspectable.get('content_type') in self.addables

    def add_account(self, registry, username, password):
        acc = self['accounts'].get(username, None)
        if acc is None:
            entry = registry.content.create(
                'XtrAccount',
                **dict(username=username, password=password)
            )
            self['accounts'][username] = entry
        elif acc.password != password:
            acc.password = password
        return

    def grab_attr(self, string, attr):
        idx = string.find(attr)
        return string[idx:].split('"')[1] if idx > 0 else ''

    def parse_m3u(self, fp, **kwargs):
        cat_to_skip = kwargs.get('cat_to_skip', list())
        ret = []
        groups = set()
        c_tag = '#EXTINF:-1 '
        lines = fp.readlines()
        m3u_cat = 'UNDEF'
        for i in range(len(lines)):
            li = lines[i].decode('utf8').replace("\r", "").replace("\n", "")
            if li.startswith(c_tag):
                channel_name = li[::-1].split(',')[0][::-1]
                extra = li.replace(channel_name, '')
                _channel_name = channel_name.split(':')
                if len(_channel_name) > 1:
                    channel_name = ':'.join(_channel_name[1:])
                i += 1
                li = lines[i].decode('utf8').replace("\r", "").replace("\n", "")
                if channel_name.startswith(('*', '=', '#', '----')):
                    print('%s skipped marked as a cat' % channel_name)
                    m3u_cat = channel_name.replace('-', '').replace('*', '').replace('=', '')
                    m3u_cat = m3u_cat.replace(' ', '').replace('#', '')
                else:
                    tvg_id = self.grab_attr(extra, 'tvg-id')
                    logo = self.grab_attr(extra, 'tvg-logo')
                    tvg_name = self.grab_attr(extra, 'tvg-name')
                    grp_title = self.grab_attr(extra, 'group-title')
                    attrs = {
                        'name': channel_name,
                        'url': li.replace('\n', ''),
                        'tvg_logo': logo if not logo.startswith('data:') else '',
                        'tvg_name': tvg_name,
                        'tvg_id': tvg_id,
                        'group_title': grp_title,
                        'related': [m3u_cat],
                        'number': i // 2
                    }
                    if attrs['url'].replace(' ', '').endswith(('.avi', '.mkv', '.mp4', '.flv')):
                        attrs['related'].append('VOD MOVIE')
                    ret.append(attrs)
                    groups.add(grp_title)
        return ret, groups

    def retrieve(self, registry, username, password):
        provider_url = 'http://%s:%d/' % (self.domain, self.port)
        path = xtr_path % (username, password)
        user_url = provider_url + path
        print('retrieving %s' % user_url)
        try:
            playlist_dict, tvg_groups = get_from_url(user_url, apply=self.parse_m3u)
            for channel_dict in playlist_dict:
                try:
                    stream = channel_dict['url'].split('/')[-1]
                    current = self.get(stream, None)
                    if not current:
                        entry = registry.content.create(
                            'ChannelMapping',
                            **channel_dict
                        )
                        self[stream] = entry
                    elif dict(current) != channel_dict:
                        # update entry
                        raise NotImplementedError('Not yet implemented (%s)' % stream)
                except Exception as e:
                    print('could not add m3u channel %s: %s' % (channel_dict.get('name', 'unknown'), str(e)))
            self.add_account(registry, username, password)
            transaction.commit()

        except Exception as e:
            print('could not retrieve %s: %s' % (provider_url, str(e)))


