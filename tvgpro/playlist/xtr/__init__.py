from persistent.list import PersistentList
from substanced.content import content
from substanced.folder import Folder


@content(
    'MediaSource',
    icon='glyphicon glyphicon-book',
)
# @implementer(IM3USourceContent)
class MediaSource(Folder):
    addables = ['XtrProvider']

    def __init__(self, **kwargs):
        super(MediaSource, self).__init__()
        self.channels = PersistentList(kwargs.pop('channels', []))

    def __sdi_addable__(self, context, introspectable):
        return introspectable.get('content_type') in self.addables
