import colander
import deform
import deform.widget
from persistent import Persistent
from persistent.list import PersistentList
from persistent.mapping import PersistentMapping
from pyramid.httpexceptions import HTTPFound
from substanced.content import content
from substanced.folder import Folder
from substanced.form import FormView
from substanced.interfaces import ReferenceType
from substanced.objectmap import find_objectmap
from substanced.objectmap import multireference_sourceid_property
from substanced.property import PropertySheet
from substanced.schema import (
    Schema,
    NameSchemaNode,
    MultireferenceIdSchemaNode)
from substanced.sdi import mgmt_view
from substanced.util import (
    oid_of,
)
from substanced.util import (
    renamer,
    find_catalog,
)
from zope.interface import (
    Interface,
    implementer,
)

from tvgpro.utils import get_country_list

shift = 100000


def channel_related(context, request):
    related = list(zip(map(str, range(shift, shift + len(context.related))), context.related))
    return related + get_country_list(context, request)


class ChannelMappingSchema(Schema):
    name = NameSchemaNode(
        editing=lambda c, r: r.registry.content.istype(c, 'ChannelMapping')
    )
    url = colander.SchemaNode(
        colander.String()
    )
    tvg_logo = colander.SchemaNode(
        colander.String(),
        missing='',
    )
    tvg_name = colander.SchemaNode(
        colander.String(),
        missing='',
    )
    tvg_id = colander.SchemaNode(
        colander.String(),
        missing='',
    )
    group_title = colander.SchemaNode(
        colander.String(),
        missing='',
    )
    number = colander.SchemaNode(
        colander.Int()
    )
    countries = MultireferenceIdSchemaNode(
        choices_getter=get_country_list,
        title='Countries',
    )


class ChannelMappingPropertySheet(PropertySheet):
    schema = ChannelMappingSchema()


@content(
    'ChannelMapping',
    icon='glyphicon glyphicon-play',
    # add_view='add_programme', 
    propertysheets=(
            ('', ChannelMappingPropertySheet),
    ),
    catalog=True,
)
class ChannelMapping(Persistent):

    def __init__(self, **kwargs):
        super(ChannelMapping, self).__init__()
        for attr, val in kwargs.items():
            setattr(self, attr, val)
            # self[attr] = val


class ChannelMappingSequenceSchema(colander.SequenceSchema):
    channel = ChannelMappingSchema()


class ChannelSequenceSchema(colander.Schema):
    channels = ChannelMappingSequenceSchema(
        widget=deform.widget.SequenceWidget(orderable=True)
    )


class ChannelSequencePropertySheet(PropertySheet):
    schema = ChannelSequenceSchema()
