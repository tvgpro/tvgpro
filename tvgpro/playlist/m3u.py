from datetime import datetime
from urllib.parse import urlparse, parse_qs

import colander
import deform
import deform.widget
import transaction
from defusedxml import ElementTree
from persistent.list import PersistentList
from persistent.mapping import PersistentMapping
from pyramid.httpexceptions import HTTPFound
from substanced.content import content
from substanced.folder import Folder, SequentialAutoNamingFolder
from substanced.form import FormView
from substanced.interfaces import ReferenceType
from substanced.objectmap import find_objectmap
from substanced.objectmap import multireference_sourceid_property
from substanced.property import PropertySheet
from substanced.schema import (
    Schema,
    NameSchemaNode,
    MultireferenceIdSchemaNode)
from substanced.sdi import mgmt_view
from substanced.util import (
    oid_of,
    find_index)
from substanced.util import (
    renamer,
    find_catalog,
)
from zope.interface import (
    Interface,
    implementer,
)

from tvgpro import get_from_url
from tvgpro.playlist.channel import ChannelSequencePropertySheet

cat3 = {
    'ADULT ': 'ADULT',
    'AF |': 'AFRICAN',
    'ARM-': 'ARMENIA',
    'ARM -': 'ARMENIA',
    'AR -': 'ARABIC',
    'AT -': 'Austria',
    'BE -': 'BELGIUM',
    'BU -': 'BULGARIA',
    'DE |': 'GERMANY',
    'DE -': 'GERMANY',
    'DISH -': 'GERMAN',
    'EXYU -': 'EX_YOUGOSLAVIA',
    'ES -': 'SPAIN',
    'FI |': 'FINLAND',
    'IT:': 'ITALIAN',
    'KU -': 'KURD',
    'KV -': 'KOSOVO',
    'MA |': 'MAROC',
    'NL -': 'NETHERLAND',
    'NO |': 'NORVEGIAN',
    'OPENTV': 'OPENTV',
    'OSN -': 'OSN',
    'PT |': 'PORTUGAL',
    'PT -': 'PORTUGAL',
    'PL |': 'POLAND',
    'PL -': 'POLAND',
    'RO |': 'ROMANIA',
    'RO -': 'ROMANIA',
    'SP -': 'SPAIN',
    'TR -': 'TURKISH',
    'RUS -': 'RUSSIA',
    'UK HD -': 'UK',
    # 'Radio': 'RADIO',
    'USA -': 'USA'
}

cat_alias = {
    # 'DE|': 'GERMAN',
    'arabi': 'ARABIC',
    'arttv': 'ARABIC',
    'af|': 'AFRICAN',
    'austria': 'Austria',
    'austira': 'Austria',
    'alger': 'ALGERIA',
    'france': 'FRENCH',
    'french': 'FRENCH',
    'gree': 'GREEK',
    'CSAT': 'FRENCH',
    'Espagne': 'SPAIN',
    'spain': 'SPAIN',
    ' nl ': 'NETHERLAND',
    'egypt': 'ARABIC',
    'jordan': 'ARABIC',
    'qatar': 'ARABIC',
    'yemen': 'ARABIC',
    'oman': 'ARABIC',
    'libya': 'ARABIC',
    'kuwait': 'ARABIC',
    'iraq': 'ARABIC',
    'lebanon': 'ARABIC',
    'bahrain': 'ARABIC',
    'bein_movies': 'ARABIC',
    'beinmovies': 'ARABIC',
    'beinsports': 'ARABIC',
    'islam': 'ARABIC',
    'romani': 'ROMANIA',
    'rotana': 'ARABIC',
    'tunis': 'ARABIC',
    'maroc': 'ARABIC',
    'morroc': 'ARABIC',
    'abudhabi': 'ARABIC',
    'uaetv': 'ARABIC',
    'maroctv': 'ARABIC',
    'mbc tv': 'MBC',
    'osn tv': 'OSN',
    'portugal': 'PORTUGAL',
    'russia': 'RUSSIA',
    'belg': 'BELGIUM_SWISS',
    'swiss': 'BELGIUM_SWISS',
    'switz': 'BELGIUM_SWISS',
    'united kingdom': 'UK',
    'scandinavia': 'SCANDINAVIAN',
    'skyuk': 'UK',
    # 'United Kingdom TV': 'UK',
    'skyitaly': 'ITALY',
    'italy': 'ITALY',
    'usatv': 'USA',
    'india': 'INDIA',
    'usa': 'USA',
    'russia': 'RUSSIA',
    'germany': 'GERMAN'
}


class IM3USourceContent(Interface):
    pass


class M3USourceParsedSchema(Schema):
    domain = colander.SchemaNode(
        colander.String(),
    )
    port = colander.SchemaNode(
        colander.Int(),
        missing='80'
    )
    username = colander.SchemaNode(
        colander.String(),
    )
    password = colander.SchemaNode(
        colander.String(),
    )


def is_an_http_url(node, value):
    result = urlparse(value)
    if result.scheme != 'http':  # and result.path == '/get.php':
        """
        args = parse_qs(result.query)
        type = args['type']
        user = args['username']
        password = args['password']
        result = dict(host=result.hostname, port=result.port, username=user, password=password, type=type)
        """
        raise colander.Invalid(node, 'you must provide an http url')


class M3USourceSchema(Schema):
    url = colander.SchemaNode(
        colander.String(),
        validator=is_an_http_url
    )


class M3USourcePropertySheet(PropertySheet):
    schema = M3USourceSchema()


def m3u_src_columns(folder, subobject, request, default_columnspec):
    subobject_name = getattr(subobject, '__name__', str(subobject))
    objectmap = find_objectmap(folder)
    user_oid = getattr(subobject, '__creator__', None)
    m3u_group = getattr(subobject, 'group_title', None)
    if user_oid is not None:
        user = objectmap.object_for(user_oid)
        user_name = getattr(user, '__name__', 'anonymous')
    else:
        user_name = 'anonymous'

    def make_sorter(index_name):
        def sorter(folder, resultset, limit=None, reverse=False):
            index = find_index(folder, 'sdidemo', index_name)
            if index is None:
                return resultset
            return resultset.sort(index, limit=limit, reverse=reverse)

        return sorter

    return default_columnspec + [
        {'name': 'Title',
         'value': getattr(subobject, 'name', subobject_name),
         'sorter': make_sorter('title'),
         },
        {'name': 'Group',
         'value': m3u_group,
         },
        {'name': 'Creator',
         'value': user_name,
         }
    ]


def account_from_url(node, value):
    result = urlparse(value)
    if result.scheme == 'http':  # and result.path == '/get.php':
        args = parse_qs(result.query)
        _type = args['type']
        user = args['username'][0]
        password = args['password'][0]
        return dict(host=result.hostname, port=result.port, username=user, password=password, type=_type)


@content(
    'M3USource',
    icon='glyphicon glyphicon-book',
    add_view='add_m3u_source',
    propertysheets=(
            ('Basic', M3USourcePropertySheet),
            ('Channels', ChannelSequencePropertySheet),
    ),
    columns=m3u_src_columns,
)
@implementer(IM3USourceContent)
class M3USource(Folder):
    addables = ['M3UChannel']

    def __init__(self, url, **kwargs):
        super(M3USource, self).__init__()
        self.url = url
        self.channels = PersistentList(kwargs.pop('channels', []))
        acc = account_from_url(self, url)
        if acc:
            for attr, val in acc.items():
                setattr(self, attr, val)

    def __sdi_addable__(self, context, introspectable):
        return introspectable.get('content_type') in self.addables

    def retrieve(self, registry):
        provider_url = self.url

        def apply(fp, **kwargs):
            cat_to_skip = kwargs.get('cat_to_skip', list())
            ret = []
            groups = set()
            c_tag = '#EXTINF:-1 '
            lines = fp.readlines()
            m3u_cat = 'UNDEF'
            for i in range(len(lines)):
                li = lines[i].decode('utf8').replace("\r", "").replace("\n", "")
                if li.startswith(c_tag):
                    channel_name = li[::-1].split(',')[0][::-1]
                    extra = li.replace(channel_name, '')
                    _channel_name = channel_name.split(':')
                    if len(_channel_name) > 1:
                        channel_name = ':'.join(_channel_name[1:])
                    i += 1
                    li = lines[i].decode('utf8').replace("\r", "").replace("\n", "")
                    if channel_name.startswith(('*', '=', '#', '----')):
                        print('%s skipped marked as a cat' % channel_name)
                        m3u_cat = channel_name.replace('-', '').replace('*', '').replace('=', '')
                        m3u_cat = m3u_cat.replace(' ', '').replace('#', '')
                    else:
                        idx = extra.find('tvg-id')
                        tvg_id = extra[idx:].split('"')[1] if idx > 0 else ''
                        idx = extra.find('tvg-logo')
                        logo = extra[idx:].split('"')[1] if idx > 0 else ''
                        idx = extra.find('tvg-name')  # group-title
                        tvg_name = extra[idx:].split('"')[1] if idx > 0 else ''
                        idx = extra.find('group-title')
                        grp_title = extra[idx:].split('"')[1] if idx > 0 else ''
                        attrs = {
                            'name': channel_name,
                            'url': li.replace('\n', ''),
                            'tvg_logo': logo if not logo.startswith('data:') else '',
                            'tvg_name': tvg_name,
                            'tvg_id': tvg_id,
                            'group_title': grp_title,
                            'related': [m3u_cat],
                            'number': i // 2
                        }
                        if attrs['url'].replace(' ', '').endswith(('.avi', '.mkv', '.mp4', '.flv')):
                            attrs['related'].append('VOD MOVIE')
                        for kw, alias in cat3.items():
                            if channel_name.startswith(kw):
                                attrs['related'].append(alias)
                        for keyword, alias in cat_alias.items():
                            if keyword.lower() in grp_title.lower():
                                attrs['related'].append(alias)
                            elif keyword.lower() in m3u_cat.lower() or keyword.lower() in tvg_name.lower():
                                attrs['related'].append(alias)
                        ret.append(attrs)
                        groups.add(grp_title)
            return ret, groups

        print('retrieving %s' % provider_url)
        try:
            playlist_dict, tvg_groups = get_from_url(provider_url, apply=apply)
            channel_len = len(playlist_dict)
            name_format = '%0' + str(len(str(channel_len))) + 'd'
            # name_format = '%0%dd' % channel_len
            for tvg in tvg_groups:
                pass
            for channel_dict in playlist_dict:
                # _id = channel_dict.pop('name')
                _id = name_format % channel_dict.get('number')
                entry = registry.content.create(
                    'ChannelMapping',
                    **channel_dict
                )
                # self.channels.append(entry)
                try:
                    self[_id] = entry
                    pass
                except Exception as e:
                    print('could not add m3u channel %s: %s' % (_id, str(e)))
            transaction.commit()

        except Exception as e:
            print('could not retrieve %s: %s' % (provider_url, str(e)))


@mgmt_view(
    name='add_m3u_source',
    tab_title='Add M3USource',
    permission='sdi.add-content',
    renderer='substanced.sdi:templates/form.pt',
    tab_condition=False,
)
class AddM3USourceView(FormView):
    title = 'Add m3u Source'
    schema = M3USourceSchema()
    buttons = ('add',)

    def add_success(self, appstruct):
        registry = self.request.registry
        binder = registry.content.create('M3USource', **appstruct)
        binder.__creator__ = oid_of(self.request.user)
        self.context.add_next(binder)
        return HTTPFound(self.request.mgmt_path(binder, '@@properties'))


