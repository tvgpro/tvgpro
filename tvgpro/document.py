import logging

import colander
import deform.widget

from persistent import Persistent

from pyramid.httpexceptions import HTTPFound
from pyramid.renderers import get_renderer
from pyramid.view import view_config

from substanced.sdi import mgmt_view
from substanced.form import FormView
from substanced.interfaces import IFolder
from substanced.content import content
from substanced.property import PropertySheet
from substanced.util import renamer
from substanced.schema import (
    Schema,
    NameSchemaNode
)


log = logging.getLogger(__name__)


# ##########################
# ###   Content stuff    ###
# ##########################

def context_is_a_document(context, request):
    return request.registry.content.istype(context, 'Document')


class DocumentSchema(Schema):
    name = NameSchemaNode(
        editing=context_is_a_document,
    )
    title = colander.SchemaNode(
        colander.String(),
    )
    body = colander.SchemaNode(
        colander.String(),
        widget=deform.widget.RichTextWidget()
    )


class DocumentPropertySheet(PropertySheet):
    schema = DocumentSchema()


@content(
    'Document',
    icon='glyphicon glyphicon-align-left',
    add_view='add_document',
)
class Document(Persistent):
    name = renamer()

    def __init__(self, title='', body=''):
        self.title = title
        self.body = body


# ##########################
# ###  Management Views  ###
# ##########################

#
#   SDI "add" view for documents
#
@mgmt_view(
    context=IFolder,
    name='add_document',
    tab_title='Add Document',
    permission='sdi.add-content',
    renderer='substanced.sdi:templates/form.pt',
    tab_condition=False,
)
class AddDocumentView(FormView):
    title = 'Add Document'
    schema = DocumentSchema()
    buttons = ('add',)

    def add_success(self, appstruct):
        registry = self.request.registry
        name = appstruct.pop('name')
        document = registry.content.create('Document', **appstruct)
        self.context[name] = document
        return HTTPFound(
            self.request.sdiapi.mgmt_path(self.context, '@@contents')
        )


# ######################
# ###  Retail Views  ###
# ######################

#
#   "Retail" view for documents.
#
@view_config(
    context=Document,
    renderer='templates/document.pt',
)
def document_view(context, request):
    return {'title': context.title,
            'body': context.body,
            'master': get_renderer('templates/master.pt').implementation(),
            }


# ######################
# ###   More Stuff   ###
# ######################

# ######################
# ###      Last      ###
# ######################

def includeme(config):  # pragma: no cover
    config.add_propertysheet('Basic', DocumentPropertySheet, Document)
