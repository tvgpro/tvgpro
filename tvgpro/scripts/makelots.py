import logging
import os
import datetime
from functools import reduce
from urllib.parse import urlparse, parse_qs

import pycountry
import transaction
from optparse import OptionParser

from substanced.folder import Folder

from tvgpro.xmltv.file import get_country_list

log = logging.getLogger(__name__)

LOREM_IPSUM = """\
Lorem ipsum dolor sit amet, consectetur adipisicing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate 
velit esse cillum dolore eu fugiat nulla pariatur."""

racacax_src = {
    'xmltv': ['FRANCE', 'BELGIUM', 'Switzerland']
}

portal_src = {
    'austria': {'AUSTRIA', 'Switzerland'},
    'belgium': {'BELGIUM'},
    'france': {'FRANCE'},
    'germany': {'GERMANY'},
    'greece': {'GREECE'},
    'italy': {'ITALY'},
    'netherlands': {'Netherlands'},
    'poland': {'POLAND'},
    'portugal': {'PORTUGAL'},
    'romania': {'ROMANIA'},
    'russia': {'Russian Federation'},
    'spain': {'SPAIN'},
    'sweden': {'SWEDEN'},
    'switzerland': {'Switzerland'},
    'turkey': {'TURKEY'},
    'usa': {'United States'},
    'uk': {'United Kingdom'}
}
estiptv_src = {
    'FR': ['FRANCE'],
    'DE': ['GERMANY'],
    'ES': ['SPAIN'],
    'IT': ['ITALY'],
    'NL': ['Netherlands'],
    'PT': ['PORTUGAL'],
    'PL': ['POLAND'],
    'RU': ['Russian Federation'],
    'TR': ['TURKEY'],
    'UK': ['United Kingdom'],
    'US': ['United States'],
}

rytec_src = {
    'rytecBE_FR_Common': {'FRANCE', 'BELGIUM'},
    'rytecBE_FR_Basic': {'FRANCE', 'BELGIUM'},
    'rytecTNT_Basic': {'FRANCE'},
    'rytecFR_Mixte': {'FRANCE'},
    'rytecFR_SportMovies': {'FRANCE'},
    'rytecUK_Basic': {'United Kingdom'},
    'rytecUK_SkyLive': {'United Kingdom'},
    'rytecUK_SkyDead': {'United Kingdom'},
    'rytecUK_SportMovies': {'United Kingdom'},
    'rytecUK_FTA': {'United Kingdom'},
    'rytecUK_int': {'United Kingdom'},
    'rytecES_Basic': {'SPAIN'},
    'rytecES_Misc': {'SPAIN'},
    'rytecES_SportMovies': {'SPAIN'},
    'rytecIT_Sky': {'ITALY'},
    'rytecIT_Basic': {'ITALY'},
    'rytecIT_SportMovies': {'ITALY'},
    'rytecNL_Basic': {'Netherlands'},
    'rytecNL_Extra': {'Netherlands'},
    'rytecPL_Basic': {'POLAND'},
    'rytecPL_Misc': {'POLAND'},
    'rytecPL_SportMovies': {'POLAND'},
    'rytecRO_Basic': {'ROMANIA'},
    'rytecGR_Basic': ['GREECE'],
    'rytecGR_SportMovies': ['GREECE'],
    'rytecPT': ['PORTUGAL'],
    'rytecCH_Basic': ['Switzerland'],
    'rytecDE_Basic': ['GERMANY'],
    'rytecDE_SportMovies': ['GERMANY'],
    'rytecDE_Common': ['GERMANY']
}

xmltv_providers_all = {
    'portal': {
        'url_template': 'http://epg.clientportal.link/%s.xml.gz',
        'comment': ' ',
        'files': portal_src,
    },
    'estiptv': {
        'url_template': 'http://estiptv.site/epg/%s_guide.xml.gz',
        'comment': ' ',
        'files': estiptv_src,
    },
    'rytec': {
        'url_template': 'http://rytec.ricx.nl/epg_data/%s.xz',
        'comment': ' ',
        'files': rytec_src,
    },
    'racacax': {
        'url_template': 'http://racacaxtv.ga/xmltv/%s.xml.gz',
        'comment': ' ',
        'files': racacax_src,
    }
}
xmltv_providers = xmltv_providers_all.copy()
# xmltv_providers.pop('portal')
# xmltv_providers.pop('estiptv')
# xmltv_providers.pop('rytec')
# xmltv_providers.pop('racacax')


def import_xmltv_sources(root, registry, **providers):
    dir_name = 'xmltv_providers'
    xmltv_folder = root.get(dir_name, None)
    if not xmltv_folder:
        root[dir_name] = Folder()
        xmltv_folder = root[dir_name]
    for provider_name, provider in list(providers.items()):
        url_template = provider['url_template']
        for src, countries in list(provider['files'].items()):
            try:
                name = '_'.join([provider_name, src])
                url = url_template % src
                country_codes = []
                for country in countries:
                    c_name = country.title()
                    py_country = pycountry.countries.get(name=c_name)
                    country_codes.append(py_country.numeric)
                args = {'url': url, 'countries': country_codes}
                entry = registry.content.create(
                    'XmlTvSource',
                    **args
                )
                xmltv_folder[name] = entry
                # entry.retrieve(registry)
            except Exception as e:
                log.debug('error adding %s: %s' % (provider_name, e))


def import_xmltv_providers(root, registry, **providers):
    xmltv_folder = root.get('xmltv', None)
    if not xmltv_folder:
        root['xmltv'] = Folder()
        xmltv_folder = root['xmltv']
    for name, provider in providers.items():
        countries = set(reduce(lambda a, b: set(a).union(b), provider['files'].values()))
        from tvgpro import CATEGORIES
        d = countries.difference(CATEGORIES)
        if len(d) > 0:
            print('Unknown Countries %s in %s' % (d, name))
        entry = registry.content.create(
            'XmlTvProvider',
            **provider
        )
        try:
            xmltv_folder[name] = entry
        except Exception as e:
            log.debug('error adding %s: %s' % (name, e))


def main():
    from pyramid.paster import bootstrap
    parser = OptionParser(description=__doc__, usage='usage: %prog [options]')
    parser.add_option('-c', '--config', dest='config',
                      help='Specify a paster config file.')
    parser.add_option('-i', '--num', dest='num', default='10',
                      help='Specify the number of blog entries to add.')
    options, args = parser.parse_args()
    config = options.config
    num = int(options.num)
    if config is None:
        raise ValueError('must supply config file name')
    config = os.path.abspath(os.path.normpath(config))

    env = bootstrap(config)
    root = env['root']
    registry = env['registry']
    closer = env['closer']
    settings = registry.settings
    import_xmltv_providers(root, registry, **xmltv_providers)
    try:
        for n in range(0, num):
            print("adding", n)
            entry = registry.content.create(
                'Blog Entry',
                'Title of blog entry %s' % n,
                LOREM_IPSUM,
                'html',
                datetime.datetime.now(),
            )
            name_id = 'blogentry_%s' % n
            root[name_id] = entry
            if n % 10000 == 0:
                print('committing')
                transaction.commit()
    except:
        pass
    print('committing')
    transaction.commit()
    root._p_jar._db.close()
    closer()


if __name__ == '__main__':
    main()
