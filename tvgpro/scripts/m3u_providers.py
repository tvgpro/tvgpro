import logging
from urllib.parse import urlparse, parse_qs

from substanced.folder import Folder

log = logging.getLogger(__name__)
accounts = [
    # ('provider_name', 'username', 'password'),
]

# account = accounts[1]
xtr_path = 'get.php?username=%s&password=%s&type=m3u_plus&output=ts'

m3u_src = {
    # 'provider_name': 'url_template_format'
}


def import_xtr_sources(root, registry, **providers):
    from tvgpro.playlist.xtr.provider import account_from_url
    ct = registry.content
    m3u_folder = root.get('m3u_providers', None)
    if not m3u_folder:
        root['m3u_providers'] = Folder()
        m3u_folder = root['m3u_providers']
    for m3u_provider, username, password in accounts:
        log.debug('processing %s' % m3u_provider)
        m3u_url = m3u_src[m3u_provider] % (username, password)
        acc = account_from_url(None, m3u_url)
        entry = registry.content.create(
            'XtrProvider',
            **dict(domain=acc['host'], port=acc['port'])
        )
        entry.retrieve(registry, acc['username'], acc['password'])
        m3u_folder[m3u_provider] = entry
        log.debug('finished %s' % m3u_provider)


def import_m3u_sources(root, registry, **providers):
    m3u_folder = root.get('playlists', None)
    if not m3u_folder:
        root['m3u_source'] = Folder()
        m3u_folder = root['m3u_source']
    for m3u_provider, username, password in accounts:
        log.debug('processing %s' % m3u_provider)
        m3u_url = m3u_src[m3u_provider] % (username, password)
        entry = registry.content.create(
            'M3USource',
            **dict(url=m3u_url)
        )
        entry.retrieve(registry)
        m3u_folder[m3u_provider] = entry
        log.debug('finished %s' % m3u_provider)
