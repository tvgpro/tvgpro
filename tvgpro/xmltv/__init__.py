

# ######################
# ###      Last      ###
# ######################
# from tvgpro.xmltv.file import XmltvFilePropertySheet


def includeme(config):  # pragma: no cover
    from tvgpro.xmltv.provider import XmlTvProviderPropertySheet, XmlTvProviderMappingPropertySheet, XmlTvProvider
    config.add_propertysheet('Basic', XmlTvProviderPropertySheet, XmlTvProvider)
    config.add_propertysheet('Files', XmlTvProviderMappingPropertySheet, XmlTvProvider)
    # UserGroupsPropertySheet XmlTvFile
    # config.add_propertysheet('Groups', XmltvFilePropertySheet, XmlTvProvider)
    from tvgpro.xmltv.file import XmlTvFile
    from tvgpro.xmltv.file import XmltvFilePropertySheet
    config.add_propertysheet('Groups', XmltvFilePropertySheet, XmlTvFile)

