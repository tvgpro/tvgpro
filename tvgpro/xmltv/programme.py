import colander
import deform
from persistent import Persistent
from substanced.content import content
from substanced.property import PropertySheet
from substanced.schema import Schema
from substanced.objectmap import find_objectmap
from substanced.util import find_index


class XmlTvProgrammeSchema(Schema):
    channel_id = colander.SchemaNode(
        colander.String(),
    )
    start = colander.SchemaNode(
        colander.DateTime(),
    )
    stop = colander.SchemaNode(
        colander.DateTime(),
    )
    xml = colander.SchemaNode(
        colander.String(),
        widget=deform.widget.RichTextWidget(readonly=True)
    )


class XmlTvProgrammePropertySheet(PropertySheet):
    schema = XmlTvProgrammeSchema()


@content(
    'XmlTvProgramme',
    icon='glyphicon glyphicon-time',
    # add_view='add_programme',
    propertysheets=(
            ('', XmlTvProgrammePropertySheet),
    ),
    catalog=True,
)
class XmlTvProgramme(Persistent):
    def __init__(self, channel_id, start, stop, xml):
        self.channel_id = channel_id
        self.start = start
        self.stop = stop
        self.xml = xml

"""
def includeme(config):
    config.add_indexview(
        IndexViews,
        catalog_name='xmltv',
        index_name='title',
        attr='title'
    )
    config.add_indexview(
        IndexViews,
        catalog_name='xmltv',
        index_name='created',
        attr='created'
    )
    config.add_indexview(
        IndexViews,
        catalog_name='xmltv',
        index_name='modified',
        attr='modified'
    )
"""