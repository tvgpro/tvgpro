from urllib.parse import urlparse

import colander
import deform
import deform.widget
from pyramid.httpexceptions import HTTPFound
from substanced.content import content
from substanced.folder import Folder
from substanced.form import FormView
from substanced.interfaces import ReferenceType
from substanced.objectmap import find_objectmap
from substanced.property import PropertySheet
from substanced.schema import (
    Schema,
    NameSchemaNode,
)
from substanced.sdi import mgmt_view
from substanced.util import (
    oid_of,
)
from substanced.util import (
    renamer,
    find_catalog,
)
from zope.interface import (
    Interface,
    implementer,
)


class IXmlTvChannelContent(Interface):
    pass


@colander.deferred
def deferred_objectrefs(node, kw):
    context = kw['context']
    root = kw['request'].root
    L = []
    maximum = 50
    current = 0
    for name, obj in root.items():
        oid = obj.__oid__
        L.append((oid, name))
        current += 1
        if current >= maximum:
            break
    return deform.widget.Select2Widget(values=L)


def is_an_http_url(node, value):
    result = urlparse(value)
    if result.scheme != 'http':
        raise colander.Invalid(node, 'you must provide an http url')


class XmlTvChannelSchema(Schema):
    # id
    name = NameSchemaNode(
        editing=lambda c, r: r.registry.content.istype(c, 'XmlTvChannel'),
    )
    display_name = colander.SchemaNode(
        colander.String(),
    )
    lang = colander.SchemaNode(
        colander.String(),
        missing='',
    )
    icon_url = colander.SchemaNode(
        colander.String(),
        missing='',
        validator=is_an_http_url
    )
    """
    raw_xml = colander.SchemaNode(
        colander.String(),
        widget=deform.widget.RichTextWidget()
    )
    """
    raw_xml = colander.SchemaNode(
        colander.String(),
        validator=colander.Length(max=500),
        widget=deform.widget.TextAreaWidget(rows=10, cols=60),
        description='Raw Xml data')


class XmlTvChannelPropertySheet(PropertySheet):
    schema = XmlTvChannelSchema()

    def get(self):
        result = PropertySheet.get(self)
        return result

    def set(self, appstruct):
        result = PropertySheet.set(self, appstruct)
        return result


def find_index(context, catalog_name, index_name):
    catalog = find_catalog(context, catalog_name)
    return catalog[index_name]


def channel_columns(folder, subobject, request, default_columnspec):
    subobject_name = getattr(subobject, '__name__', str(subobject))
    objectmap = find_objectmap(folder)
    user_oid = getattr(subobject, '__creator__', None)
    start = getattr(subobject, 'start', None)
    stop = getattr(subobject, 'stop', None)
    if user_oid is not None:
        user = objectmap.object_for(user_oid)
        creator = getattr(user, '__name__', 'anonymous')
    else:
        creator = 'anonymous'
    if start is not None:
        start = start.isoformat()
    if stop is not None:
        stop = stop.isoformat()

    return default_columnspec + [
        {'name': 'start',
         'value': start,
         'formatter': 'date',
         },
        {'name': 'end',
         'value': stop,
         'formatter': 'date',
         },
        {'name': 'Creator',
         'value': creator,
         }
    ]


@content(
    'XmlTvChannel',
    icon='glyphicon glyphicon-book',
    add_view='add_xmltv_channel',
    propertysheets=(
            ('Basic', XmlTvChannelPropertySheet),
            # ('Another', AnotherPropertySheet),
    ),
    columns=channel_columns,
)
@implementer(IXmlTvChannelContent)
class XmlTvChannel(Folder):
    name = renamer()

    def __init__(self, display_name, lang, icon_url, raw_xml):
        super(XmlTvChannel, self).__init__()
        self.display_name = display_name
        self.lang = lang
        self.icon_url = icon_url
        self.raw_xml = raw_xml

    def to_xml(self):
        return

    def from_badgerfish(self, rr):
        # extract info from badgerfished xml
        _ch = rr['channel']
        dn = _ch.get('display-name', None)
        _dn = dn.get('$', None)
        _lg = dn.get('@lang')
        pass


@mgmt_view(
    name='add_xmltv_channel',
    tab_title='Add XmlTvChannel',
    permission='sdi.add-content',
    renderer='substanced.sdi:templates/form.pt',
    tab_condition=False,
)
class AddXmlTvChannelView(FormView):
    title = 'Add XmlTv Source'
    schema = XmlTvChannelSchema()
    buttons = ('add',)

    def add_success(self, appstruct):
        registry = self.request.registry
        name = appstruct.pop('name')
        binder = registry.content.create('XmlTvChannel', **appstruct)
        binder.__creator__ = oid_of(self.request.user)
        self.context[name] = binder
        return HTTPFound(self.request.mgmt_path(binder, '@@properties'))
