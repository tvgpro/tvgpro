import gzip
import logging
import lzma
from collections import defaultdict
from multiprocessing.pool import ThreadPool

import colander
from deform import widget
from defusedxml.ElementTree import parse as defused_xml_parse
from persistent import Persistent
from persistent.list import PersistentList
from persistent.mapping import PersistentMapping
from pyramid.httpexceptions import HTTPFound
from pyramid.renderers import get_renderer
from pyramid.view import view_config
from substanced.content import content
from substanced.form import FormView
from substanced.property import PropertySheet
from substanced.schema import (
    Schema,
    NameSchemaNode
)
from substanced.sdi import mgmt_view
from substanced.util import renamer

from tvgpro import CATEGORIES
from tvgpro import get_from_url

log = logging.getLogger(__name__)


# ##########################
# ###   Files stuff    ###
# ##########################
class FileGroupMappingSchema(colander.MappingSchema):
    filename = colander.SchemaNode(colander.String())
    groups = colander.SchemaNode(
        colander.List(),
        validator=colander.Length(min=1),
        widget=widget.CheckboxChoiceWidget(
            values=sorted(zip(CATEGORIES, CATEGORIES)),
            inline=True,
        ),
    )


class FileMappingListSchema(colander.SequenceSchema):
    file_mapping = FileGroupMappingSchema()


class FilesWrapSchema(Schema):
    schema = FileMappingListSchema(widget=widget.SequenceWidget(orderable=False))


class XmlTvProviderMappingPropertySheet(PropertySheet):
    schema = FilesWrapSchema().bind(
    )

    def get(self):
        group_mapping = getattr(self.context, 'groups', {})
        ret = defaultdict(set)
        deform_att = list()
        for group, files in group_mapping.items():
            for f in files:
                ret[f].add(group)
        for file, groups in ret.items():
            deform_att.append({
                'name': file,
                'filename': file,
                'groups': str(tuple(groups)),
            })

        return {'schema': deform_att}

    def set(self, struct, omit=()):
        from pyramid.compat import is_nonstr_iter
        if not is_nonstr_iter(omit):
            omit = (omit,)
        changed = False
        for child in self.schema:
            name = child.name
            if (name in struct) and not (name in omit):
                existing_files = getattr(self.context, 'files', {})
                new_val = struct[name]
                ret = {}
                for _struct in new_val:
                    groups, filename = _struct['groups'], _struct['filename'],
                    ret[filename] = groups
                changed = ret != dict(existing_files)
                if changed:
                    self.context.update_files(ret)
                    changed = True
        return changed


# ##########################
# ###   Content stuff    ###
# ##########################

def context_is_a_xmltv_provider(context, request):
    return request.registry.content.istype(context, 'XmlTvProvider')


class XmlTvProviderSchema(Schema):
    name = NameSchemaNode(
        editing=context_is_a_xmltv_provider,
    )
    url_template = colander.SchemaNode(
        colander.String(),
    )
    comment = colander.SchemaNode(
        colander.String(),
        widget=widget.RichTextWidget()
    )


class XmlTvProviderPropertySheet(PropertySheet):
    schema = XmlTvProviderSchema()


@content(
    'XmlTvProvider',
    icon='glyphicon glyphicon-align-left',
    add_view='add_xmltv_provider',
)
class XmlTvProvider(Persistent):
    name = renamer()

    def __init__(self, url_template='', comment='', files=dict()):
        self.url_template = url_template
        self.comment = comment
        self.files = PersistentMapping()
        self.groups = PersistentMapping()
        self.update_files(files)

    def update_files(self, files):
        self.files.clear()
        for file, groups in files.items():
            self.files[file] = PersistentList(groups)
        self.groups.clear()
        for file, groups in files.items():
            for group in groups:
                self.groups[group] = self.groups.get(group, PersistentList())
                self.groups[group].append(file)

    def retrieve_xml_channels(self):
        from xmljson import badgerfish as bf
        provider_url = self.url_template
        provider_src = self.files.keys()

        def retrieve(name):
            def apply(fp, **kwargs):
                with opener(fp) as g:
                    root = defused_xml_parse(g).getroot()
                    channel_et = root.findall('channel')
                    ret = {}
                    for channel in channel_et:
                        _id = channel.attrib.get('id')
                        rr = bf.data(channel)
                        _ch = rr['channel']
                        dn = _ch.get('display-name', None)
                        _dn = dn.get('$', None)
                        _lg = dn.get('@lang')
                        d_name = channel.find('display-name')
                        if d_name is not None:
                            lang = d_name.attrib.get('lang', None)
                            display_name = d_name.text
                            # print(_id, display_name, lang)
                            ret[_id] = {
                                'display-name': display_name,
                                'lang': lang,
                                'pgm': []
                            }
                        else:
                            print(name, '- no display name for id ', _id)
                    pgr_et = root.findall('programme')
                    for pgm in pgr_et:
                        rr = bf.data(pgm)
                        # json_pgm = dumps(rr)
                        _pgm = rr['programme']
                        channel_id = _pgm.pop('@channel')
                        start = _pgm.get('@start').split(' ')
                        stop = _pgm.get('@stop').split(' ')
                        start_time, gmt = start[0], '+0000'
                        if len(start) == 2:
                            gmt = start[1]
                        _pgm.update({'@start': start_time, '@stop': stop[0], 'gmt': gmt})
                        ret[channel_id]['pgm'].append(_pgm)

                    # ret = set(map(lambda p: p.attrib.get('channel'), pgr_et))
                    # print(dict(ret))
                    g.close()
                return root

            log.info('retrieving %s' % provider_url % name)
            extension = provider_url[-3:]
            try:
                opener = open
                if extension.endswith('.xz'):
                    opener = lzma.open
                if extension.endswith('.gz'):
                    opener = gzip.open
                xml_root = get_from_url(provider_url % name, apply=apply)
                pgr = xml_root.findall('programme')
                print(len(pgr))
            except Exception as e:
                print('could not retrieve %s: %s' % (name, str(e)))
                log.error('could not retrieve %s: %s' % (name, str(e)))

        pool = ThreadPool(1)
        pool.map(retrieve, provider_src)
        # pool.join()
        pool.close()


# ##########################
# ###  Management Views  ###
# ##########################

#
#   SDI "add" view for xmltv_providers
#
@mgmt_view(
    # context=IFolder,
    name='add_xmltv_provider',
    tab_title='Add XmlTvProvider',
    permission='sdi.add-content',
    renderer='substanced.sdi:templates/form.pt',
    tab_condition=False,
)
class AddXmlTvProviderView(FormView):
    title = 'Add new XmlTv Provider'
    schema = XmlTvProviderSchema()
    buttons = ('add',)

    def add_success(self, appstruct):
        registry = self.request.registry
        name = appstruct.pop('name')
        xmltv_provider = registry.content.create('XmlTvProvider', **appstruct)
        xmltv_provider.packages = PersistentMapping()
        self.context[name] = xmltv_provider
        return HTTPFound(
            self.request.sdiapi.mgmt_path(self.context, '@@contents')
        )


# ######################
# ###  Retail Views  ###
# ######################

#
#   "Retail" view for xmltv_providers.
#
@view_config(
    context=XmlTvProvider,
    renderer='tvgpro:templates/document.pt',
)
def xmltv_provider_view(context, request):
    context.retrieve_xml_channels()
    return {'title': context.name,
            'body': context.comment,
            'master': get_renderer('tvgpro:templates/master.pt').implementation(),
            }

# ######################
# ###   More Stuff   ###
# ######################
