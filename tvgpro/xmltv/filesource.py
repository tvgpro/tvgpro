import logging
from datetime import datetime

import colander
import deform
import deform.widget
import transaction
from defusedxml import ElementTree
from persistent.list import PersistentList
from persistent.mapping import PersistentMapping
from pyramid.httpexceptions import HTTPFound
from substanced.content import content
from substanced.folder import Folder
from substanced.form import FormView
from substanced.interfaces import ReferenceType
from substanced.objectmap import find_objectmap
from substanced.objectmap import multireference_sourceid_property
from substanced.property import PropertySheet
from substanced.schema import (
    Schema,
    NameSchemaNode,
    MultireferenceIdSchemaNode)
from substanced.sdi import mgmt_view
from substanced.util import (
    oid_of,
)
from substanced.util import (
    renamer,
    find_catalog,
)
from zope.interface import (
    Interface,
    implementer,
)

from tvgpro import get_from_url
from tvgpro.xmltv.file import get_country_list

log = logging.getLogger(__name__)

class IXmlTvSourceContent(Interface):
    pass


def context_is_a_binder(context, request):
    return request.registry.content.istype(context, 'XmlTvSource')


class XmlTvSourceSchema(Schema):
    name = NameSchemaNode(
        editing=lambda c, r: r.registry.content.istype(c, 'XmlTvFile'),
    )
    url = colander.SchemaNode(
        colander.String(),
    )
    countries = MultireferenceIdSchemaNode(
        choices_getter=get_country_list,
        title='Countries',
    )


class XmlTvSourcePropertySheet(PropertySheet):
    schema = XmlTvSourceSchema()

    def get(self):
        result = PropertySheet.get(self)
        return result

    def set(self, appstruct):
        result = PropertySheet.set(self, appstruct)
        return result


def find_index(context, catalog_name, index_name):
    catalog = find_catalog(context, catalog_name)
    return catalog[index_name]


def binder_columns(folder, subobject, request, default_columnspec):
    subobject_name = getattr(subobject, '__name__', str(subobject))
    objectmap = find_objectmap(folder)
    user_oid = getattr(subobject, '__creator__', None)
    created = getattr(subobject, '__created__', None)
    modified = getattr(subobject, '__modified__', None)
    if user_oid is not None:
        user = objectmap.object_for(user_oid)
        user_name = getattr(user, '__name__', 'anonymous')
    else:
        user_name = 'anonymous'
    if created is not None:
        created = created.isoformat()
    if modified is not None:
        modified = modified.isoformat()

    def make_sorter(index_name):
        def sorter(folder, resultset, limit=None, reverse=False):
            index = find_index(folder, 'sdidemo', index_name)
            if index is None:
                return resultset
            return resultset.sort(index, limit=limit, reverse=reverse)

        return sorter

    return default_columnspec + [
        {'name': 'Title',
         'value': getattr(subobject, 'title', subobject_name),
         'sorter': make_sorter('title'),
         },
        {'name': 'Modified Date',
         'value': modified,
         'sorter': make_sorter('modified'),
         'formatter': 'date',
         },
        {'name': 'Creator',
         'value': user_name,
         }
    ]


def xmltv_src_buttons(context, request, default_buttons):
    buttons = [
                  {'type': 'single',
                   'buttons':
                       [
                           {'id': 'xmltvsrcupdate',
                            'name': 'form.xmltvsrcupdate',
                            'class': 'btn-primary btn-sdi-act',
                            'value': 'xmltvsrcupdate',
                            'text': 'Update'}
                       ]
                   },
                  {'type': 'single',
                   'buttons':
                       [
                           {'id': 'xmltvsrcclean',
                            'name': 'form.cleanpgm',
                            'class': 'btn-primary btn-sdi-act',
                            'value': 'xmltvsrcclean',
                            'text': 'Clean'}
                       ]
                   }
              ] + default_buttons
    return buttons


@content(
    'XmlTvSource',
    icon='glyphicon glyphicon-book',
    add_view='add_xmltv_source',
    propertysheets=(
            ('Basic', XmlTvSourcePropertySheet),
            # ('Another', AnotherPropertySheet),
    ),
    buttons=xmltv_src_buttons,
    columns=binder_columns,
)
@implementer(IXmlTvSourceContent)
class XmlTvSource(Folder):
    name = renamer()
    addables = ['XmlTvChannel']

    def __init__(self, url, countries):
        super(XmlTvSource, self).__init__()
        self.url = url
        self.countries = countries

    def __sdi_addable__(self, context, introspectable):
        return introspectable.get('content_type') in self.addables

    def retrieve(self, registry):
        provider_url = self.url

        def apply(fp, **kwargs):
            with opener(fp) as g:
                from defusedxml.ElementTree import parse as xml_parse
                root = xml_parse(g).getroot()
                channels = root.findall('channel')
                programmes = root.findall('programme')
                g.close()
                log.debug('finished parsing xml')
                return channels, programmes

        extension = provider_url[-3:]
        opener = open
        if extension.endswith('.xz'):
            import lzma
            opener = lzma.open
        if extension.endswith('.gz'):
            import gzip
            opener = gzip.open
        channel_et, pgr = get_from_url(provider_url, apply=apply)
        try:
            log.debug('start adding channels')
            for channel in channel_et:
                """
                @ todo: use badgerfish serialisation
                from xmljson import badgerfish as bf
                _ch = bf.data(channel)['channel']
                dn = _ch.get('display-name', None)
                _dn = dn.get('$', None)
                """
                _id = channel.attrib.get('id').replace('/', '_')
                d_name = channel.find('display-name')
                if d_name is not None:
                    lang = d_name.attrib.get('lang', '')
                    channel_args = {
                        'display_name': d_name.text,
                        'lang': lang,
                        'icon_url': '',
                        'raw_xml': str(ElementTree.tostring(channel, method='xml'))
                    }
                    existing = self.get(_id, None)
                    if not existing:
                        entry = registry.content.create(
                            'XmlTvChannel',
                            **channel_args
                        )
                        self[_id] = entry
                else:
                    print(provider_url, '- no display name for id ', _id)
            transaction.commit()
            log.debug('finished adding channels')
            log.debug('start adding pgm')
            for programme in pgr:
                pgr_channel = programme.attrib.get('channel').replace('/', '_')
                pgr_start = programme.attrib.get('start').replace('/', '_')
                pgr_stop = programme.attrib.get('stop').replace('/', '_')
                date_format = '%Y%m%d%H%M%S %z'
                # @todo add only if now < pgr_start
                # now = datetime.datetime.now().replace(microsecond=0)
                # @todo handle tzinfo
                pgr_attrs = {
                    'channel_id': pgr_channel,
                    'start': datetime.strptime(pgr_start, date_format).replace(tzinfo=None),
                    'stop': datetime.strptime(pgr_stop, date_format).replace(tzinfo=None),
                    'xml': str(ElementTree.tostring(programme, method='xml'))
                }
                existing = self[pgr_channel].get(pgr_start, None)
                if not existing:  # or existing.xml != pgr_attrs['xml']
                    entry = registry.content.create(
                        'XmlTvProgramme',
                        **pgr_attrs
                    )
                    self[pgr_channel][pgr_start] = entry
                elif pgr_attrs['xml'] == self[pgr_channel][pgr_start].xml:
                    log.debug('%s programme starting at %s already exists' % (pgr_channel, pgr_start))
                    pass
            log.debug('finished adding pgm')
            transaction.commit()

        except Exception as e:
            log.info('could not retrieve %s: %s' % (provider_url, str(e)))


@mgmt_view(
    name='add_xmltv_source',
    tab_title='Add XmlTvSource',
    permission='sdi.add-content',
    renderer='substanced.sdi:templates/form.pt',
    tab_condition=False,
)
class AddXmlTvSourceView(FormView):
    title = 'Add XmlTv Source'
    schema = XmlTvSourceSchema()
    buttons = ('add',)

    def add_success(self, appstruct):
        registry = self.request.registry
        name = appstruct.pop('name')
        binder = registry.content.create('XmlTvSource', **appstruct)
        binder.__creator__ = oid_of(self.request.user)
        self.context[name] = binder
        return HTTPFound(self.request.mgmt_path(binder, '@@properties'))


@mgmt_view(
    # context=IXmlTvSourceContent,
    content_type='XmlTvSource',
    name='contents',
    request_param='form.xmltvsrcupdate',
    request_method='POST',
    renderer='substanced.folder:templates/contents.pt',
    permission='sdi.manage-contents',
    tab_condition=False,
    )
def update_content(context, request):
    context.retrieve(request.registry)
    log.debug('finished update xmltv content')
    return HTTPFound(request.sdiapi.mgmt_path(context, '@@contents'))


@mgmt_view(
    # context=IXmlTvSourceContent,
    content_type='XmlTvSource',
    name='contents',
    request_param='form.cleanpgm',
    request_method='POST',
    renderer='substanced.folder:templates/contents.pt',
    permission='sdi.manage-contents',
    tab_condition=False,
    )
def clean_programme(context, request):
    now = datetime.now().replace(microsecond=0)
    for channel in context.values():
        count = 0
        for pgm in channel.values():
            if pgm.start < now:
                del channel[pgm.__name__]
                # channel.pop(pgm.__name__)
                count += 1
        if len(channel) == 0:
            del context[channel.__name__]
            # context.pop(channel.__name__)
        log.debug("%d pgm deleted for %s" % (count, channel.__name__))
    log.debug('finished cleaning %s' % context.__name__)
    return HTTPFound(request.sdiapi.mgmt_path(context, '@@contents'))
