import colander
import pycountry
from persistent import Persistent
from substanced.content import content
from substanced.folder import Folder
from substanced.folder.views import AddFolderView
from substanced.interfaces import IFolder
from substanced.property import PropertySheet
from substanced.schema import Schema, MultireferenceIdSchemaNode, NameSchemaNode
from substanced.sdi import mgmt_view
from substanced.util import renamer

from tvgpro import CATEGORIES
from tvgpro.utils import get_country_list


class XmltvFileSchema(Schema):
    """ Restricted schema for :class:`substanced.principal.User` objects.
    """
    name = NameSchemaNode(
        editing=lambda c, r: r.registry.content.istype(c, 'XmlTvFile')
    )
    file_url = colander.SchemaNode(
        colander.String()
    )
    groupids = MultireferenceIdSchemaNode(
        choices_getter=get_country_list,
        title='Groups',
    )


class XmltvFilePropertySheet(PropertySheet):
    schema = XmltvFileSchema()
    permissions = (
        ('view', 'sdi.manage-user-groups'),
        ('change', 'sdi.manage-user-groups'),
    )

    def get(self):
        pass


def channels_columns(folder, subobject, request, default_columnspec):
    pubdate = getattr(subobject, 'pubdate', None)
    if pubdate is not None:
        pubdate = pubdate.isoformat()

    return default_columnspec + [
        {'name': 'Publication date',
         'value': pubdate,
         'formatter': 'date',
         },
    ]


@content(
    'XmlTvFile',
    icon='glyphicon glyphicon-list',
    add_view='add_xmltv_file',
    propertysheets=(
            ('', XmltvFilePropertySheet),
    ),
    # columns=channels_columns,
)
class XmlTvFile(Folder):
    """ Folder for comments of a blog entry
    """
    name = renamer()
    addables = ['XmlTvProgramme']

    def __init__(self, file_url, groupids):
        Folder.__init__(self)
        self.file_url = file_url
        self.groupids = groupids

    def __sdi_addable__(self, context, introspectable):
        return introspectable.get('content_type') in self.addables


@mgmt_view(
    context=IFolder,
    name='add_xmltv_file',
    tab_title='Add XmlTv File',
    permission='sdi.add-content',
    renderer='substanced.sdi:templates/form.pt',
    # content_type='XmlTvFile',
    tab_condition=False,
)
class AddXmlTvFileView(AddFolderView):
    title = 'Add new XmlTv File'
    buttons = ('add',)
    schema = XmltvFileSchema()
