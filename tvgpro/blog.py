from webob.exc import HTTPFound
from substanced.sdi import mgmt_view
from substanced.form import FormView

import colander
import datetime
import time
import deform.widget

from persistent import Persistent
from pyramid.security import (
    Allow,
    Everyone,
)

from substanced.content import content
from substanced.folder import Folder
from substanced.property import PropertySheet
from substanced.root import Root
from substanced.schema import (
    Schema,
    NameSchemaNode,
)
from substanced.util import renamer

import datetime
import pytz

from docutils.core import publish_parts
from webob import Response

from pyramid.decorator import reify
from pyramid.httpexceptions import HTTPFound
from pyramid.url import resource_url
from substanced.util import find_catalog
from pyramid.view import (
    view_config,
    view_defaults,
)


def _getentrybody(format, entry):
    if format == 'rst':
        body = publish_parts(entry, writer_name='html')['fragment']
    else:
        body = entry
    return body


@view_defaults(
    content_type='Blog Entry',
    renderer='templates/blog/blogentry.pt',
)
class BlogEntryView(object):

    def __init__(self, context, request):
        self.context = context
        self.request = request

    @reify
    def blogentry(self):
        return _getentrybody(self.context.format, self.context.entry)

    @reify
    def comments(self):
        context = self.context
        system_catalog = find_catalog(context, 'system')
        blog_catalog = find_catalog(context, 'blog')
        content_type = system_catalog['content_type']
        path = system_catalog['path']
        comments_path = self.request.resource_path(context['comments'])
        query = content_type.eq('Comment') & path.eq(comments_path)
        query_result = query.execute().sort(blog_catalog['pubdate'])
        return query_result

    @reify
    def attachments(self):
        return self.context['attachments'].values()

    @view_config(request_method='GET')
    def view_blogentry(self):
        return dict(error_message='')

    @view_config(request_method='POST')
    def add_comment(self):
        params = self.request.params
        commenter_name = params.get('commenter_name')
        comment_text = params.get('comment_text')
        spambot = params.get('spambot')
        if spambot:
            message = 'Your comment could not be posted'
        elif comment_text == '' and commenter_name == '':
            message = 'Please enter your name and a comment'
        elif comment_text == '':
            message = 'Please enter a comment'
        elif commenter_name == '':
            message = 'Please enter your name'
        else:
            pubdate = datetime.datetime.now()
            comment = self.request.registry.content.create(
                'Comment', commenter_name, comment_text, pubdate)
            self.context.add_comment(comment)
            return HTTPFound(location=self.request.resource_url(self.context))

        return dict(error_message=message)


@view_config(
    content_type='File',
    name='download',
)
def download_attachment(context, request):
    f = context.blob.open()
    headers = [('Content-Type', str(context.mimetype)),
               ('Content-Disposition',
                'attachment;filename=%s' % str(context.__name__)),
               ]
    response = Response(headerlist=headers, app_iter=f)
    return response


@view_defaults(
    content_type='Root',
)
class FeedViews(object):
    def __init__(self, context, request):
        self.context = context
        self.request = request

    def _nowtz(self):
        now = datetime.datetime.utcnow()  # naive
        y, mo, d, h, mi, s = now.timetuple()[:6]
        return datetime.datetime(y, mo, d, h, mi, s, tzinfo=pytz.utc)

    def _get_feed_info(self):
        context = self.context
        request = self.request
        feed = {"rss_url": request.application_url + "/rss.xml",
                "atom_url": request.application_url + "/index.atom",
                "blog_url": request.application_url,
                "title": context.sdi_title,
                "description": context.description,
                }

        def _add_updated_strings(updated, info):
            if getattr(updated, 'now', None) is None:
                y, mo, d, h, mi, s = updated.timetuple()[:6]
                updated = datetime.datetime(y, mo, d, h, mi, s, tzinfo=pytz.utc)
            info['updated_atom'] = updated.astimezone(pytz.utc).isoformat()
            info['updated_rss'] = updated.strftime('%a, %d %b %Y %H:%M:%S %z')

        blogentries = []
        for name, blogentry in context.items():
            if request.registry.content.istype(blogentry, 'Blog Entry'):
                updated = blogentry.pubdate
                info = {'url': resource_url(blogentry, request),
                        'title': blogentry.title,
                        'body': _getentrybody(blogentry.format,
                                              blogentry.entry),
                        'created': updated,
                        'pubdate': updated,
                        }
                _add_updated_strings(updated, info)
                blogentries.append((updated, info))

        blogentries.sort(key=lambda x: x[0].isoformat())
        blogentries = [entry[1] for entry in reversed(blogentries)][:15]
        updated = blogentries and blogentries[0]['pubdate'] or self._nowtz()
        _add_updated_strings(updated, feed)

        return feed, blogentries

    @view_config(
        name='rss.xml',
        renderer='templates/blog/rss.pt',
    )
    def blog_rss(self):
        feed, blogentries = self._get_feed_info()
        self.request.response.content_type = 'application/rss+xml'
        return dict(
            feed=feed,
            blogentries=blogentries,
        )

    @view_config(
        name='index.atom',
        renderer='templates/blog/atom.pt',
    )
    def blog_atom(self):
        feed, blogentries = self._get_feed_info()
        self.request.response.content_type = 'application/atom+xml'
        return dict(
            feed=feed,
            blogentries=blogentries,
        )


@colander.deferred
def now_default(node, kw):
    return datetime.datetime.now()


class BlogEntrySchema(Schema):
    name = NameSchemaNode(
        editing=lambda c, r: r.registry.content.istype(c, 'Blog Entry')
    )
    title = colander.SchemaNode(
        colander.String(),
    )
    entry = colander.SchemaNode(
        colander.String(),
        widget=deform.widget.TextAreaWidget(rows=20, cols=70),
    )
    format = colander.SchemaNode(
        colander.String(),
        validator=colander.OneOf(['rst', 'html']),
        widget=deform.widget.SelectWidget(
            values=[('rst', 'rst'), ('html', 'html')]),
    )
    pubdate = colander.SchemaNode(
        colander.DateTime(default_tzinfo=None),
        default=now_default,
    )


class BlogEntryPropertySheet(PropertySheet):
    schema = BlogEntrySchema()


@content(
    'Blog Entry',
    icon='glyphicon glyphicon-book',
    add_view='add_blog_entry',
    propertysheets=(
            ('', BlogEntryPropertySheet),
    ),
    catalog=True,
    # tab_order=('properties', 'acl_edit'),
)
class BlogEntry(Folder):
    name = renamer()

    def __init__(self, title, entry, format, pubdate):
        Folder.__init__(self)
        self.title = title
        self.entry = entry
        self.format = format
        self.pubdate = pubdate
        self['attachments'] = Attachments()
        self['comments'] = Comments()

    def __sdi_addable__(self, context, introspectable):
        return False

    def add_comment(self, comment):
        while 1:
            name = str(time.time())
            if not name in self:
                self['comments'][name] = comment
                break


@mgmt_view(
    # context='Root',
    content_type='Root',
    name='add_blog_entry',
    permission='sdi.add-content',
    renderer='substanced.sdi:templates/form.pt',
    tab_condition=False,
)
class AddBlogEntryView(FormView):
    title = 'Add Blog Entry'
    schema = BlogEntrySchema()
    buttons = ('add',)

    def add_success(self, appstruct):
        name = appstruct.pop('name')
        request = self.request
        blogentry = request.registry.content.create('Blog Entry', **appstruct)
        self.context[name] = blogentry
        loc = request.mgmt_path(self.context, name, '@@properties')
        return HTTPFound(location=loc)


class CommentSchema(Schema):
    commenter_name = colander.SchemaNode(
        colander.String(),
    )
    text = colander.SchemaNode(
        colander.String(),
    )
    pubdate = colander.SchemaNode(
        colander.DateTime(),
        default=now_default,
    )


class CommentPropertySheet(PropertySheet):
    schema = CommentSchema()


@content(
    'Comment',
    icon='glyphicon glyphicon-comment',
    add_view='add_comment',
    propertysheets=(
            ('', CommentPropertySheet),
    ),
    catalog=True,
)
class Comment(Persistent):
    def __init__(self, commenter_name, text, pubdate):
        self.commenter_name = commenter_name
        self.text = text
        self.pubdate = pubdate


def comments_columns(folder, subobject, request, default_columnspec):
    pubdate = getattr(subobject, 'pubdate', None)
    if pubdate is not None:
        pubdate = pubdate.isoformat()

    return default_columnspec + [
        {'name': 'Publication date',
         'value': pubdate,
         'formatter': 'date',
         },
    ]


@content(
    'Comments',
    icon='glyphicon glyphicon-list',
    columns=comments_columns,
)
class Comments(Folder):
    """ Folder for comments of a blog entry
    """

    def __sdi_addable__(self, context, introspectable):
        return introspectable.get('content_type') == 'Comment'


def attachments_columns(folder, subobject, request, default_columnspec):
    kb_size = None
    if getattr(subobject, 'get_size', None) and callable(subobject.get_size):
        kb_size = int(int(subobject.get_size()) / 1000)

    return default_columnspec + [
        {'name': 'Size',
         'value': "%s kB" % kb_size,
         },
    ]


@content(
    'Attachments',
    icon='glyphicon glyphicon-list',
    columns=attachments_columns,
)
class Attachments(Folder):
    """ Folder for attachments of a blog entry
    """

    def __sdi_addable__(self, context, introspectable):
        return introspectable.get('content_type') == 'File'
