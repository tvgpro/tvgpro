from webob.exc import HTTPFound
from substanced.sdi import mgmt_view
from substanced.form import FormView

import colander
import datetime
import time
import deform.widget

from persistent import Persistent
from pyramid.security import (
    Allow,
    Everyone,
)

from substanced.content import content
from substanced.folder import Folder
from substanced.property import PropertySheet
from substanced.root import Root
from substanced.schema import (
    Schema,
    NameSchemaNode,
)
from substanced.util import renamer

import datetime
import pytz

from docutils.core import publish_parts
from webob import Response

from pyramid.decorator import reify
from pyramid.httpexceptions import HTTPFound
from pyramid.url import resource_url
from substanced.util import find_catalog
from pyramid.view import (
    view_config,
    view_defaults,
)

from tvgpro.blog import _getentrybody


class BlogSchema(Schema):
    """ The schema representing the blog root. """
    title = colander.SchemaNode(
        colander.String(),
        missing=''
    )
    description = colander.SchemaNode(
        colander.String(),
        missing=''
    )


class BlogPropertySheet(PropertySheet):
    schema = BlogSchema()


def blog_columns(folder, subobject, request, default_columnspec):
    title = getattr(subobject, 'title', None)
    pubdate = getattr(subobject, 'pubdate', None)
    if pubdate is not None:
        pubdate = pubdate.isoformat()

    return default_columnspec + [
        {'name': 'Title',
         'value': title,
         },
        {'name': 'Publication Date',
         'value': pubdate,
         'formatter': 'date',
         },
    ]


@view_config(
    renderer='tvgpro:templates/blog/frontpage.pt',
    content_type='Root',
)
def blog_view(context, request):
    system_catalog = find_catalog(context, 'system')
    blog_catalog = find_catalog(context, 'blog')
    content_type = system_catalog['content_type']
    query = content_type.eq('Blog Entry')
    query_result = query.execute().sort(blog_catalog['pubdate'], reverse=True)
    blog_entries = []
    for blog_entry in query_result:
        blog_entries.append(
            {'url': resource_url(blog_entry, request),
             'title': blog_entry.title,
             'body': _getentrybody(blog_entry.format, blog_entry.entry),
             'pubdate': blog_entry.pubdate,
             'attachments': [
                 {'name': a.__name__, 'url': resource_url(a, request, 'download')}
                 for a in blog_entry['attachments'].values()],
             'numcomments': len(blog_entry['comments'].values()),
             })
    return dict(blogentries=blog_entries)


@content(
    'Root',
    icon='glyphicon glyphicon-home',
    propertysheets=(
            ('', BlogPropertySheet),
    ),
    after_create=('after_create', 'after_create_blog'),
    columns=blog_columns,
)
class Blog(Root):
    title = 'Tvg Blog'
    description = 'Tvg Blog Description '
    addables = ['Blog Entry']

    def __sdi_addable__(self, context, introspectable):
        return True or introspectable.get('content_type') in self.addables

    @property
    def sdi_title(self):
        return self.title

    @sdi_title.setter
    def sdi_title(self, value):
        self.title = value

    def after_create_blog(self, inst, registry):
        acl = getattr(self, '__acl__', [])
        acl.append((Allow, Everyone, 'view'))
        self.__acl__ = acl
        catalogs = self['catalogs']
        # catalogs.add_catalog('blog', update_indexes=True)
        catalogs.add_catalog('playlist', update_indexes=True)
        print('start importing m3u sources')
        from tvgpro.scripts.m3u_providers import import_m3u_sources
        from tvgpro.scripts.m3u_providers import import_xtr_sources
        import_xtr_sources(self, registry)
        # import_m3u_sources(self, registry)
        print('finished importing m3u sources')
        catalogs.add_catalog('xmltv', update_indexes=True)
        from tvgpro.scripts.makelots import xmltv_providers, import_xmltv_sources, import_xmltv_providers
        print('start importing xmltv providers')
        import_xmltv_sources(self, registry, **xmltv_providers)
        print('finished importing xmltv providers')


