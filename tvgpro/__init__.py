from pyramid.config import Configurator
from substanced.db import root_factory
import logging

CATEGORIES = set()

log = logging.getLogger(__name__)


def get_from_url(url, apply=None, **kwargs):
    log.info('retrieving %s' % url)
    try:
        import urllib.request
        with urllib.request.urlopen(url) as fp:
            if apply:
                ret = apply(fp, **kwargs)
                fp.close()
                return ret
            return fp
    except Exception as e:
        log.error('Could not retrieve %s:\n   %s\n' % (url, e))


def cron_task(system):
    registry = system['registry']
    request = system['request']
    # do stuff


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings, root_factory=root_factory)
    config.include('substanced')
    config.include('pyramid_retry')
    config.include('pyramid_tm')
    settings = config.get_settings()
    global CATEGORIES
    CATEGORIES.update(settings.get('categories', '').split(' '))
    log.info('using categories \n %s' % CATEGORIES)
    print('including xmltv')
    config.include('tvgpro.xmltv')
    # config.include('tvgpro.playlist')
    print('scanning')
    config.scan()
    config.add_static_view('static', 'static', cache_max_age=86400)
    # config.add_cron_task(cron_task, hour=(0, 24, 3))
    return config.make_wsgi_app()
