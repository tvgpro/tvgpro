import datetime

import colander
from hypatia.query import InRange, Comparator
from pyramid.view import view_config
import logging

from substanced.util import find_catalog, coarse_datetime_repr

log = logging.getLogger(__name__)


@colander.deferred
def now_default(node, kw):
    return datetime.datetime.now()

# ######################
# ###  Retail Views  ###
# ######################

#
#   Default "retail" view
#
@view_config(
    renderer='templates/splash.pt',
)
def splash_view(request):
    manage_prefix = request.registry.settings.get('substanced.manage_prefix',
                                                  '/manage')
    return {'manage_prefix': manage_prefix}


@view_config(name='ctest')
def ctest(context, request):
    channel = request.params.get('channel', '13eRue')
    demo_catalog = find_catalog(context, 'xmltv')
    system_catalog = find_catalog(context, 'system')
    import datetime
    # @todo handle tzinfo from user and compare real dates with hypathia (maybe coarse_datetime_repr)
    date_format = '%Y-%m-%d %H:%M:%S'
    now = datetime.datetime.now().replace(microsecond=0)
    expired = (
        # demo_catalog['start'].lt(str(now))
        # system_catalog['content_type'].eq('XmlTvChannel')
        demo_catalog['channel_id'].eq(channel)
        # demo_catalog['display_name'].eq(channel)
    )
    resultset = expired.execute().all()
    expired_results = list(resultset)
    expired_channels = set(map(lambda r: r.channel_id, expired_results))
    expired_start = set(map(lambda r: r.start, expired_results))
    request.response.text = str(expired_start) + str(expired_channels)

    return request.response


@view_config(name='clean_programme')
def clean_programme(context, request):
    demo_catalog = find_catalog(context, 'xmltv')
    date_format = '%Y-%m-%d %H:%M:%S'
    now = datetime.datetime.now().replace(microsecond=0)
    expired = (
        demo_catalog['start'].lt(now.strftime(date_format))
        # system_catalog['content_type'].eq('XmlTvChannel')
    )
    resultset = expired.execute().all()
    expired_results = list(resultset)
    request.response.text = '%d programmes removed' % len(expired_results)
    for r in expired_results:
        print(now.strftime(date_format),
              'removing %s programme from %s channel (id %s)' % (r.start, r.__parent__.__name__, r.channel_id))
        r.__parent__.pop(r.__name__)

    return request.response
