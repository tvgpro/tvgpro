import fuzzywuzzy


def fuzz(l1, l2, cutoff=80, simplifier=(lambda x: (x, x))):
    def apply_simplifier(l):
        return dict(map(simplifier, l))

    _l1 = apply_simplifier(l1)
    _l2 = apply_simplifier(l2)
    # keys are simplified names, values are original channel names
    m = map(lambda channel: (channel, fuzzywuzzy.process.extractOne(channel, _l2.keys(), score_cutoff=cutoff)), _l1.keys())
    d = dict()
    f = filter(lambda xx: xx is not None, map(lambda cc: (_l1[cc[0]], _l2[cc[1][0]]) if cc[1] else None, d.items()))
    return dict(f)
