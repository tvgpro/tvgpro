import pycountry


def get_country_list(context, request):
    return list(map(lambda c: (c.numeric, c.name), pycountry.countries))


