#!/bin/bash

WSGI="pyramid.wsgi"
# WSGI="oc_app"

ARGS=""
ARGS="$ARGS --request-timeout 360"
ARGS="$ARGS --startup-timeout 3600"
ARGS="$ARGS --log-level info"
ARGS="$ARGS --log-to-terminal"
ARGS="$ARGS --port 8080"

# exec mod_wsgi-express start-server $ARGS oc_app
# exec mod_wsgi-express start-server $ARGS --application-type paste openshift.ini
exec mod_wsgi-express start-server $ARGS $WSGI
