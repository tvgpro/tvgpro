import os
from pyramid.paster import get_app, setup_logging

here = os.path.dirname(os.path.abspath(__file__))
config = os.path.join(here, 'openshift.ini')
setup_logging(config)
application = get_app(config, 'main')
