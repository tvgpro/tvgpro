import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.txt')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()

requires = [
    'defusedxml',
    'waitress',
    'substanced',
    'multidict',
    'persistent',
    'deform',
    'colander',
    'fuzzywuzzy',
    'python-Levenshtein',
    'mock',
    'mod_wsgi',
    'mod_wsgi-httpd',
    'pyramid_cron',
    'watchdog',
    'paste',
    'pyramid',
    'pyramid_retry',
    'pyramid_tm',
    'pyramid_recaptcha',
    'pyramid_debugtoolbar',
    'docutils',
    'pycountry',
    'pytz',
    'transaction',
    'xmljson'
    ]

setup(name='tvgpro',
      version='0.0',
      description='tvgpro',
      long_description=README + '\n\n' +  CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pylons",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='',
      url='',
      keywords='web pyramid pylons substanced',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      tests_require=requires,
      test_suite="tvgpro",
      entry_points="""\
      [paste.app_factory]
      main = tvgpro:main
      """,
      )

